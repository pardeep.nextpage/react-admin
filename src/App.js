import React, { useEffect } from "react";
import "./theme/style.scss";
import { Outlet, useNavigate } from "react-router-dom";
import { readLocal } from "./common/Util";
import Loader from "./common/Loader";
import Header from "./containers/Header";
import Sidebar from "./containers/Sidebar";

const App = () => {
  const navigate = useNavigate();
  useEffect(() => {
    let userdata = readLocal("LoginInfo");
    if (typeof userdata !== "string") {
      navigate("/login");
    }
  }, [navigate]);

  return (
    <>
      <Sidebar />
      <Header />
      {/* <Loader /> */}
      <Outlet />
    </>
  );
};

export default App;
