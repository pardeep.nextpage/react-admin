import React from "react";
import { useNavigate } from "react-router-dom";
import { Controller, useForm } from "react-hook-form";
import {
  Button,
  Form,
  FormFeedback,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import { setLocal } from "../common/Util";

const Login = () => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      email: "",
      password: "",
    },
    mode: "onBlur",
  });

  const navigate = useNavigate();

  const onSubmit = (data) => {
    try {
      if (data.email === "user@yopmail.com" && data.password === "12345678") {
        setLocal("LoginInfo", JSON.stringify(data));
        navigate("/dashboard");
      } else {
        throw new Error("Invalid Credentials!");
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <div className="login-wrapper">
        <h2>Login</h2>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <FormGroup floating>
            <Controller
              name="email"
              control={control}
              rules={{
                required: {
                  value: true,
                  message: "This field is required!",
                },
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  message: "Invalid Email",
                },
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input
                  id="login-email"
                  placeholder="Enter Email"
                  type="email"
                  onBlur={onBlur}
                  onChange={onChange}
                  value={value}
                  invalid={!!errors.email}
                />
              )}
            />
            <Label for="login-email">Email</Label>
            <FormFeedback>{errors?.email?.message}</FormFeedback>
          </FormGroup>
          <FormGroup floating>
            <Controller
              name="password"
              control={control}
              rules={{
                required: {
                  value: true,
                  message: "This field is required!",
                },
                minLength: {
                  value: 8,
                  message: "Password cannot be less than 8 characterrs!",
                },
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input
                  id="login-password"
                  placeholder="Enter Password"
                  type="password"
                  onBlur={onBlur}
                  onChange={onChange}
                  value={value}
                  invalid={!!errors.password}
                />
              )}
            />
            <Label for="login-password">Password</Label>
            <FormFeedback>{errors?.password?.message}</FormFeedback>
          </FormGroup>
          <Button type="submit">Submit</Button>
        </Form>
      </div>
    </>
  );
};

export default Login;
