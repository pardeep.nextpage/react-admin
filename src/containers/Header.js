import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Navbar } from "react-bootstrap";
import { VscThreeBars } from "react-icons/vsc";
import { FaUserCircle } from "react-icons/fa";

const Header = () => {
  const [sidebar, setSidebar] = useState(false);
  const navigate = useNavigate();

  const logout = () => {
    localStorage.clear();
    navigate("/");
  };

  const toggleSidebar = () => {
    setSidebar(!sidebar);
  };

  return (
    <>
      <Navbar className="navbar navbar-default nav">
        <div className="container-fluid header">
          <div className="navbar-header">
            <VscThreeBars size={30} onClick={toggleSidebar} />
          </div>
          <div className="float">
            <FaUserCircle size={30} onClick={logout} />
          </div>
        </div>
      </Navbar>
    </>
  );
};

export default Header;
