import React, { useState } from "react";
import { FaHome } from "react-icons/fa";
import { FiUsers } from "react-icons/fi";

const Sidebar = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <div className="container-fluid sidebar">
        <div className="row">
          <div className="col-auto min-vh-100 text-bg-secondary">
            <ul>
              <li>
                <a className="nav-link px-2">
                  <FaHome />
                  <span className="ms-1 d-none d-sm-inline">Home</span>
                </a>
              </li>
              <li>
                <a className="nav-link px-2">
                  <FiUsers />
                  <span className="ms-1 d-none d-sm-inline">Users</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};

export default Sidebar;
